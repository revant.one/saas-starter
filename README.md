# Business Apps

Monorepo of business apps

## Prerequisites

- VSCode devcontainer

## Development

Clone the repo

```shell
git clone https://gitlab.com/revant.one/saas-starter && cd saas-starter
```

Copy the create the `.devcontainer` directory from `devcontainer-example`.

```shell
cp -R devcontainer-example .devcontainer
```

### Use VSCode Remote Containers extension

Setup [VSCode Remote - Containers extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers).

If you want to modify the services, edit `.devcontainer/docker-compose.yml`.

VSCode should automatically inquire you to install the required extensions, that can also be installed manually as follows:

- Install Remote - Containers for VSCode
    - through command line `code --install-extension ms-vscode-remote.remote-containers`
    - clicking on the Install button in the Vistual Studio Marketplace: [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
    - View: Extensions command in VSCode (Windows: Ctrl+Shift+X; macOS: Cmd+Shift+X) then search for extension `ms-vscode-remote.remote-containers`

After the extensions are installed, you can:

- Open saas-starter folder in VS Code.
    - `code .`
- Launch the command, from Command Palette (Ctrl + Shift + P) `Execute Remote Containers : Reopen in Container`. You can also click in the bottom left corner to access the remote container menu.

### Copy environment files

```shell
cp packages/backend-server/env-example packages/backend-server/.env
```

### Starting NestJS apps for development

```shell
cd packages/<app-server>
yarn start:debug
```

Note:

- `<app-server>` replace app server with any of the package.
- Copy `vscode-example` to `.vscode`, It'll make the Ctrl+Shift+P > Run Tasks to show all the packages and also setup debugger.

### Run development setup

```shell
./scripts/setup-dev \
  --admin-name "System Admin" \
  --admin-email admin@example.com \
  --admin-password Secret@1234 \
  --admin-phone +919876543210 \
  --org-name "Example Inc"
```
