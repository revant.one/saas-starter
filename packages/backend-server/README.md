### Models

#### AuditMastersModule

- AuditMastersNamingSeries
- AuditFirm (1 per Tenant)
- AuditArea
- AuditType
- AuditStage
- Contact
- Location
- OtherPermanentRecord
- Client
  - Contacts[]
  - Locations[]
  - OtherPermanentRecords[]

#### AuditTemplatesModule

- AuditTemplatesNamingSeries
- AuditAreaList
- AuditAreaListTemplate
  - AuditAreaList[]
- AuditStagesList
- AuditStagesListTemplate
  - AuditStagesList[]
- Question
- ChecklistQuestionTemplate
  - Question[]

#### AuditTransactionsModule

- AuditTransactionsNamingSeries
- AuditFileStage
- AuditFileArea
- AuditFile (Workflow)
  - AuditFileStage[]
  - AuditFileArea[]
- AuditQuery (Workflow)
- ChecklistQuestionDetails
- AuditQueryInfo
- Checklist (Workflow)
  - ChecklistQuestionDetails[]
  - AuditQueryInfo[]
