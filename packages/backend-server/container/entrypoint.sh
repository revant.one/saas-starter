#!/bin/bash

function checkEnv() {
  if [[ -z "$DB_HOST" ]]; then
    echo "DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$DB_NAME" ]]; then
    echo "DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$DB_USER" ]]; then
    echo "DB_USER is not set"
    exit 1
  fi
  if [[ -z "$DB_PASSWORD" ]]; then
    echo "DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_HOST" ]]; then
    echo "ID_DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_NAME" ]]; then
    echo "ID_DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_USER" ]]; then
    echo "ID_DB_USER is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_PASSWORD" ]]; then
    echo "ID_DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
}

function configureServer() {
  if [ ! -f .env ]; then
    env > .env
  fi
}

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Configure server
  configureServer
  # Start server
  node dist/main.js
fi

exec "$@"
