import { HttpException, HttpStatus } from '@nestjs/common';
import { SETUP_ALREADY_COMPLETE } from '../constants/messages';

export class SettingsAlreadyExistsException extends HttpException {
  constructor() {
    super(SETUP_ALREADY_COMPLETE, HttpStatus.BAD_REQUEST);
  }
}
