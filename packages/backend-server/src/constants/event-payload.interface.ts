export interface EventPayload {
  eventId: string;
  eventName: string;
  eventFromService: string;
  eventDateTime: Date;
  eventData: any;
}
