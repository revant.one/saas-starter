import { HealthIndicatorFunction } from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';

export const HEALTHCHECK_TIMEOUT = 10000;

@Injectable()
export class HealthCheckAggregateService {
  createTerminusOptions(): HealthIndicatorFunction[] {
    return [];
  }
}
