import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthAggregates } from './aggregates';
import { HealthControllers } from './controllers';

@Module({
  imports: [TerminusModule],
  controllers: [...HealthControllers],
  providers: [...HealthAggregates],
})
export class HealthcheckModule {}
