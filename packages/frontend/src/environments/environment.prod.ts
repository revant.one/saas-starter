export const environment = {
  production: true,

  // Configure Variables
  appURL: 'https://myaccount.castlecraft.in',
  authorizationURL: 'https://accounts.castlecraft.in/oauth2/confirmation',
  authServerURL: 'https://accounts.castlecraft.in',
  callbackProtocol: 'blocks',
  callbackUrl: 'https://myaccount.castlecraft.in/connected_device/callback',
  clientId: 'client_id',
  profileURL: 'https://accounts.castlecraft.in/oauth2/profile',
  revocationURL: 'https://myaccount.castlecraft.in/connected_device/revoke',
  scope: 'profile openid email roles phone',
  tokenURL: 'https://accounts.castlecraft.in/oauth2/token',
  isAuthRequiredToRevoke: true,
};

// client_id is available for public, client_secret is to be protected.
